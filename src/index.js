import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.scss';
import reportWebVitals from './reportWebVitals';
import { Route } from "wouter";
import { List } from 'views/list';
import { Detail } from 'views/detail';

ReactDOM.render(
    <React.StrictMode>
        <Route 
            path="/"
            component={List}
            data='comics'></Route>
        <Route 
            path="/comic/:idcomic"
            component={Detail} />
    </React.StrictMode>,
    document.getElementById('root')
);
reportWebVitals();
