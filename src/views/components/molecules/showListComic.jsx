import { Fragment } from "react";
import { Link } from 'wouter';
import { Image } from 'views/components/atoms/img/img';

export const ShowListComic = props => {
    const {
        title, 
        thumbnail,
        id
    } = props.data;

    return (
        <Fragment>
            <Link to={`/comic/${id}`}>
                <h3>{title}</h3>
                {thumbnail && thumbnail !== ''  ? <Image data={{image:thumbnail, title}}/> : ''}
            </Link>
        </Fragment>
    );
}