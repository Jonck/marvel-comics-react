import React, { useState, useEffect, Fragment } from "react"
import {requestApi} from 'middelware/requestApi'
import { Image } from 'views/components/atoms/img/img';

export const ShowSingleRecomended = props => {
    const {resourceURI} = props.data
    const [sourceRecomended, updateRecomended] = useState([]);
    const keyword = resourceURI.replace('http://gateway.marvel.com/v1/public/', '');

    useEffect( () => {
        const getData = async () => {
            const allData = await requestApi(keyword);
            updateRecomended(allData.results);
        };
        getData();
    }, [keyword]);

    return (
        <Fragment>
        {
            sourceRecomended.map(item => {
                const {title, thumbnail, id} = item
                console.log(item);
                return (
                    <div className="" key={`singleRecomended-${id}`}>
                        {title}
                        <Image data={{image: thumbnail, title}} />
                    </div>
                );
            })
        }
        </Fragment>
    );
}