import { Fragment } from 'react';
import { Image } from 'views/components/atoms/img/img';
import { Paragraph } from 'views/components/atoms/text/paragraph'

const CreatorsComponent = props => {
    const {creators} = props.data;
    return (
        <div className="creators">
            {creators.map( (creator, index) => {
                const {name, role} = creator;

                return(
                    <Fragment key={`autors-${index}`}>
                        <p>{name}</p>
                        <p>{role}</p>
                    </Fragment>
                );
            })}
        </div>
    )
}

export const ShowSingleComic = props => {
    const {
        comic: {
            title,
            description,
            thumbnail,
            prices,
            creators: {
                items: creators
            }
        }
    } = props.data;

    return (
        <div className="container">
            <div className="image">
                <Image data={{image: thumbnail, title}} />
            </div>
            <div className="content">
                <h3>{title}</h3>
                <Paragraph data={{description, moreClass: ''}}/>
                {creators && creators.length > 0 ? <CreatorsComponent data={{creators}} /> : '' }
                {prices && prices.length > 0 ? <p>price: ${prices[0].price}</p> : ''}
            </div>
        </div>
    );
}