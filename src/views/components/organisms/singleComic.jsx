import { Fragment, useState, useEffect } from "react";
import { requestApi } from 'middelware/requestApi';
import { ShowSingleComic } from 'views/components/molecules/showSingleComic'
import { Recommended } from './recommended';

export const SingleComicComponent = props => {
    const {idcomic} = props.data;
    const [singleComic, updateComic] = useState([]);

    useEffect(() => {
        const getData = async() => {
            const allData = await requestApi(`comics/${idcomic}`);
            updateComic(allData.results);
        }
        getData();
    }, [idcomic]);
    
    return (
        <Fragment>
            <section className="single-comic">
                {singleComic.map( comic => {
                    return (
                        <div key={`single-comic-${comic.id}`}>
                            <ShowSingleComic data={{comic}}  />
                            <Recommended data={{series: comic.series}}/>
                        </div>
                    )
                })}
            </section>
        </Fragment>
    );
}