import { useState, useEffect, Fragment } from "react";
import { requestApi } from 'middelware/requestApi';
import { ShowSingleRecomended } from 'views/components/molecules/showSingleRecomended';

export const Recommended = props => {
    const {series: {name, resourceURI: uri }} = props.data;
    const idRecommended = uri.replace('http://gateway.marvel.com/v1/public/series/', '')
    
    const [recommended, updateRecommended] = useState([]);

    useEffect( () => {
        const getData = async () => {
            const allData = await requestApi(`series/${idRecommended}`);
            updateRecommended(allData.results);
        };
        getData();
    }, [idRecommended]);

    return (
        <div className="recommended">
            <div className="container">
                <h3>{name}</h3>
                {
                    recommended.map(item => {
                        const {id, comics: {items: listRecomended}} = item;

                        return(
                            <Fragment key={`recomend-${id}`}>
                                {listRecomended.map((singleRecomended, index) => {
                                    return <ShowSingleRecomended data={singleRecomended} key={`singleRecomended-${index}`}/>
                                })}
                            </Fragment>
                        );
                    })
                }
            </div>
        </div>
    );
}