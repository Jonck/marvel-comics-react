import {useState, useEffect, Fragment} from 'react';
import { requestApi } from 'middelware/requestApi';
import { ShowListComic } from 'views/components/molecules/showListComic';

export const ListComics = props => {
    const {keyword} = props;
    const [allComics, updateComics] = useState([]);

    useEffect(() => {
        const getData = async () => {
            const allData  = await requestApi(keyword);
            updateComics(allData.results);
        }
        getData();
    }, [keyword]);
    
    console.log(allComics);

    return (
        <Fragment>
            <section className="list-comics">
                <div className="container">
                    {allComics.map( dataComic => {
                        return <ShowListComic data={dataComic} key={`list-comic-${dataComic.id}`} />
                    })}
                </div>
            </section>
        </Fragment>
    )
}