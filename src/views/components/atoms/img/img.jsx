import { Fragment } from "react";
import 'scss/components/img.scss'

export const Image = props => {
    const {image: {extension, path}, title} = props.data

    return (
        <Fragment>
            <img src={`${path}.${extension}`} alt={title}/>
        </Fragment>
    );
}
