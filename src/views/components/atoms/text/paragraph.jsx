import { Fragment } from "react"

export const Paragraph = props => {
    const {description, moreClass} = props.data;
    
    return (
        <Fragment>
            <p dangerouslySetInnerHTML={{__html: description}} className={moreClass ? moreClass : ''} />
        </Fragment>
    )
}