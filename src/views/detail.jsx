import { Fragment } from "react";
import { SingleComicComponent } from 'views/components/organisms/singleComic';

export const Detail = props => {
    const {params:{idcomic}} = props;
   
    return (
        <Fragment>
            <SingleComicComponent data={{idcomic}} />
        </Fragment>
    );
}