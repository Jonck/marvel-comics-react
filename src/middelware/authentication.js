import md5 from 'js-md5';

const apiKeyPub =  'e0fbab3496d1b03081eaac011ed7b191', //this information is usually saved in the .eve file
  apiKeyPri = 'd0b593ddcac18cce092f874fb70f68b5e6f34ccd'; //this information is usually saved in the .eve file

export const authentication = () => {
  const timeStamp = new Date().getTime();
  const hasKey = md5(timeStamp+apiKeyPri+apiKeyPub);

  return {timeStamp, apiKeyPub, hasKey}; 
}
