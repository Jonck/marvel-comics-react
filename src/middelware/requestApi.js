import {authentication} from 'middelware/authentication';

const dinamicURl = keyword => {
    const {timeStamp, apiKeyPub, hasKey} = authentication();

    return `http://gateway.marvel.com/v1/public/${keyword}?ts=${timeStamp}&apikey=${apiKeyPub}&hash=${hasKey}`
};

export const requestApi = async (keyword) => {
    var result = false;
    await fetch(dinamicURl(keyword))
        .then(res => res.json())
        .then(res => result = res.data)
        .catch(error => console.log(error));

    return result ? result : false;
}