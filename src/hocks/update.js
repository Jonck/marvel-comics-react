import  { useEffect } from 'react';
import { requestApi } from 'middelware/requestApi';

export const update = (keyword, updataData) => {
    useEffect( () => {
        const getData = async () => {
            const allData = await requestApi(keyword);
            updataData(allData.results)
        };
        getData();
    }, [keyword]);
}